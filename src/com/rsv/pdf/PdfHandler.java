/*
 *  @(#)PdfHandler.java 28 Aug, 2018
 *
 *  Copyright (c) Radiant Sage.
 *  SoftSol Building, Plot No.4,Hitech City, Hyderabad-500081,INDIA.
 *  All rights reserved.
 *
 *  This software is the confidential and proprietary information of
 *  Radiant Sage . ("Confidential Information").  You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Radiant Sage.
 */
package com.rsv.pdf;

import com.jnj.ice.jhove.JhoveModel;
import com.mis.utils.PDFUtils;
import com.mis.workflow.utils.WorkflowHelper;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.LimitBy;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.ResultSetRow;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.cmr.version.Version;
import org.alfresco.service.cmr.version.VersionHistory;
import org.alfresco.service.cmr.version.VersionService;
import org.alfresco.service.cmr.workflow.WorkflowService;
import org.alfresco.service.cmr.workflow.WorkflowTask;
import org.alfresco.service.cmr.workflow.WorkflowTaskState;
import org.alfresco.service.namespace.QName;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;

/**
 * It will handle functionality related to pdf with task
 * @author ankur
 */
public class PdfHandler extends AbstractWebScript {

    private ServiceRegistry serviceRegistry;
    private String configpath;
    private NodeService ns;
    private WorkflowService ws;
    private ContentService cs;
    private FileFolderService fs;
    private VersionService vs;

    // Logging support
    private static final Log logger = LogFactory.getLog(PdfHandler.class);
    private static final QName VISIT_QNAME = QName.createQName(JhoveModel.JHOVE_MODEL_URI, "ClinicalTrialTimePointDescription");
    private static final QName CACHE_QNAME = QName.createQName(JhoveModel.JHOVE_MODEL_URI, "CacheKey");
    private static final QName READ_TYPE_QNAME = QName.createQName(JhoveModel.JHOVE_MODEL_URI, "ReadType");

    @Override
    public void execute(WebScriptRequest req, WebScriptResponse res) throws IOException {
        InputStream is = null;
        try {
            // Services
            if (ns == null) {
                ns = serviceRegistry.getNodeService();
            }
            if (ws == null) {
                ws = serviceRegistry.getWorkflowService();
            }
            if (cs == null) {
                cs = serviceRegistry.getContentService();
            }
            if (fs == null) {
                fs = serviceRegistry.getFileFolderService();
            }
            if (vs == null) {
                vs = serviceRegistry.getVersionService();
            }

            // Handling 'taskid' request-parameter
            String taskid = req.getParameter("taskid");

            if (taskid == null || taskid.isEmpty()) {
                throw new AlfrescoRuntimeException("Exception : unable to get taskid request parameter");
            }

            WorkflowTask task = ws.getTaskById(taskid);
            

            // Getting task information
            String taskname = task.getName();
            Map<QName, Serializable> taskProps = task.getProperties();

            String subjectId = (String) taskProps.get(JhoveModel.PROP_CLINICALSUBJECTID);
            String visit = (String) taskProps.get(VISIT_QNAME);
            NodeRef trialRef = (NodeRef) taskProps.get(WorkflowHelper.TRIALREF_FOR_TASK);
            boolean isCompleted = task.getState().equals(WorkflowTaskState.COMPLETED);
            String currentuser = serviceRegistry.getAuthenticationService().getCurrentUserName();

            // Getting configuration file
            NodeRef config_file = WorkflowHelper.getRef(configpath, trialRef, ns);
            is = cs.getReader(config_file, ContentModel.PROP_CONTENT).getContentInputStream();
            Properties pro = new Properties();
            pro.load(is);
            String type_json_string = pro.getProperty("TypeKey");
            JSONObject obj = new JSONObject(type_json_string);
            String type = null;
            if (obj.has(taskname)) {
                type = obj.getString(taskname);
            } else if (obj.has("Default")) {
                type = obj.getString("Default");
            }
            
            // Getting ReadType
            String task_readtype = (String) taskProps.get(READ_TYPE_QNAME);
            boolean isReadTypeInTask = task_readtype != null && !task_readtype.isEmpty();
            String readtype_json_string = pro.getProperty("ReadTypeMap");
            JSONObject readobj = new JSONObject(readtype_json_string);
            String readtype = null;
            if (isReadTypeInTask && readobj.has(task_readtype)) {
                readtype = readobj.getString(task_readtype);
            } else if (readobj.has("Default")) {
                readtype = readobj.getString("Default");
            }

            JSONObject jobj = new JSONObject();

            // Handling Parameters
            String action = req.getParameter("action");
            String ecId = req.getParameter("eventCRFId");
            String view = req.getParameter("isView");
            boolean isview = (view != null && view.equalsIgnoreCase("true"));
            
            String reader = req.getParameter("reader");
            if (reader == null || reader.isEmpty()) {
                if (isview) {
                    throw new AlfrescoRuntimeException("Unable to get reader");
                } else {
                    reader = currentuser;
                }
            } else {
                if(!isview && !reader.equals(currentuser)){
                    throw new AlfrescoRuntimeException("Reader and CurrentUser are not being matched");
                }
            }
            
            boolean doPermanentRedact=false;
            boolean checkDrawing=false;
            
            if(action != null){
                if(action.equalsIgnoreCase("DoPermanentRedact")){
                    doPermanentRedact=true;
                }else if(action.equalsIgnoreCase("CheckTemporaryDrawing")){
                    checkDrawing=true;
                }else{
                    throw new AlfrescoRuntimeException("Unexpected value for action request-parameter");
                } 
            }
            
            // Handling values
            NodeRef pdf_file = null;
            Map<String, String> map = new HashMap<String, String>();
            map.put("ClinicalTrialSubjectID", subjectId);
            map.put("ClinicalTrialTimePointDescription", visit);
            map.put("TaskID", taskid);
            map.put("Reader", (reader==null)?"":reader);
            map.put("CrfID", (ecId==null)?"":ecId);
            map.put("ReadType", (readtype==null)?"":readtype);
            
            String searchLocation = pro.getProperty(handlePropertyName(type, "Pdf_SearchLocation"));
            searchLocation = replaceMethod(searchLocation, map);
            
            // Handling use-cases
            if (checkDrawing) {
                String pdfRef = req.getParameter("pdfRef");
                pdf_file = new NodeRef(pdfRef);
                Map<QName, Serializable> props = ns.getProperties(pdf_file);
                String status = (String) props.get(JhoveModel.PROP_STATUS);
                if (status != null && status.equalsIgnoreCase("PermanentRedact")) {
                    jobj.put("TemporaryDrawed", false);
                } else {
                    VersionHistory vh = vs.getVersionHistory(pdf_file);
                    if (vh.getAllVersions().size() == 1) {
                        jobj.put("TemporaryDrawed", false);
                    } else {
                        NodeRef space = null;
                        try{
                        space = WorkflowHelper.getRef(searchLocation, trialRef, ns);
                        }catch(Exception e){  
                        }
                        if(space == null){
                        jobj.put("TemporaryDrawed", true);    
                        }else{
                        Version version = vs.getCurrentVersion(pdf_file);
                        NodeRef version_node = version.getFrozenStateNodeRef();
                        
                        SearchParameters sp1 = new SearchParameters();
                        StoreRef storeRef = new StoreRef(StoreRef.PROTOCOL_WORKSPACE,
                                "SpacesStore");
                        String path = ns.getPath(space).toPrefixString(serviceRegistry.getNamespaceService());

                        // Preparing Query
                        String crquery = "+PATH:\"" + path + "//*\" ";
                        crquery += " +((TYPE:\"{http://www.alfresco.org/model/content/1.0}content\"))";
                        crquery += " AND @\\{http\\://www.alfresco.org/model/content/1.0\\}content.mimetype:application/pdf";
                        crquery += " AND  @\\{http\\://ice.jnj.com/model/jhove/1.0\\}info:\"" + version_node + "\"";

                        sp1.addStore(storeRef);
                        sp1.setLanguage(SearchService.LANGUAGE_LUCENE);
                        sp1.setLimitBy(LimitBy.UNLIMITED);

                        sp1.setQuery(crquery);
                        ResultSet results = serviceRegistry.getSearchService().query(sp1);
                        int size = results.length();
                        boolean b_result = false;
                        if (size == 0) {
                            b_result = true;
                        } else {
                            for (ResultSetRow row : results) {
                                NodeRef n = row.getNodeRef();
                                String val = (String) ns.getProperty(n, CACHE_QNAME);
                                if (val != null && val.equals(ecId+"_"+taskid)) {
                                    b_result = true;
                                    break;
                                }
                            }
                        }
                        jobj.put("TemporaryDrawed", b_result);
                        }
                    }
                }

            } else {

                if (!doPermanentRedact && !isCompleted) {
                    // Getting Temporary pdf-file
                    
                    String configured_pdf_path = pro.getProperty(handlePropertyName(type, "Config_PdfFile"));
                    // ReadTempLocation
                    String checking_path = pro.getProperty(handlePropertyName(type, "Pdf_TempLocation"));

                    checking_path = replaceMethod(checking_path, map);
                    NodeRef spacePath = WorkflowHelper.SpaceRef(trialRef, serviceRegistry, checking_path);
                    String[] arr = configured_pdf_path.split("/", -1);
                    pdf_file = ns.getChildByName(spacePath, ContentModel.ASSOC_CONTAINS, arr[arr.length - 1]);
                    if(!isview){
                    if (pdf_file == null) {
                        NodeRef configured_pdf_file = WorkflowHelper.getRef(configured_pdf_path, trialRef, ns);
                        pdf_file = fs.copy(configured_pdf_file, spacePath, arr[arr.length - 1]).getNodeRef();
                        ns.setProperty(pdf_file, JhoveModel.PROP_CLINICALSUBJECTID, subjectId);
                        ns.addAspect(pdf_file, ContentModel.ASPECT_VERSIONABLE, null);
                    }else{
                        String creator = (String) ns.getProperty(pdf_file, ContentModel.PROP_CREATOR);
                        if(!creator.equals(currentuser)){
                           throw new AlfrescoRuntimeException("CurrentUser and File-Creator are not same.");
                        }
                        
                    }
                    jobj.put("Editable", true);
                    }else{
                    jobj.put("Editable", false);   
                    }

                } else {
                    String target_path = pro.getProperty(handlePropertyName(type, "Pdf_TargetLocation"));
                    target_path = replaceMethod(target_path, map);
                    String fileName = pro.getProperty(handlePropertyName(type, "Pdf_TargetFileName"));
                    fileName = replaceMethod(fileName, map);
                    String complete_path = target_path + "/" + fileName;
                    try{
                    pdf_file = WorkflowHelper.getRef(complete_path, trialRef, ns);
                    }catch (Exception e) {
                        logger.error(e.getMessage());
                    }

                    Map<QName, Serializable> props = null;
                    if (doPermanentRedact) {
                        String node = req.getParameter("pdfRef");
                        NodeRef file = new NodeRef(node);
                        String gcreator = (String) ns.getProperty(file, ContentModel.PROP_CREATOR);
                        if (!gcreator.equals(currentuser)) {
                            throw new AlfrescoRuntimeException("CurrentUser and GivenFile-Creator are not same.");
                        }
                        Version version = vs.getCurrentVersion(file);
                        NodeRef version_node = version.getFrozenStateNodeRef();
                        
                        boolean updating = true;
                        if(pdf_file != null){
                            String creator = (String) ns.getProperty(pdf_file, ContentModel.PROP_CREATOR);
                            if(!creator.equals(currentuser)){
                            throw new AlfrescoRuntimeException("CurrentUser and ExistingFile-Creator are not same.");
                            }
                            String info = (String) ns.getProperty(pdf_file, JhoveModel.PROP_INFO);
                            if(info != null && info.equals(version_node.toString())){
                                updating = false;
                            }
                        }
                        if(updating){
                        NodeRef parent = ns.getPrimaryParent(file).getParentRef();
                        NodeRef redact_pdf_file = fs.copy(file, parent, fileName).getNodeRef();
                        PDFUtils.loadAndDoPermanentRedAct(serviceRegistry, redact_pdf_file, trialRef, target_path);
                        props = new HashMap<QName, Serializable>();
                        props.put(JhoveModel.PROP_INFO, version_node.toString());
                        props.put(VISIT_QNAME, visit);
                        props.put(CACHE_QNAME, ecId+"_"+taskid);
                        props.put(JhoveModel.PROP_STATUS, "PermanentRedact");
                        String title = req.getParameter("pdf_title");
                        if(title == null || title.isEmpty()){
                            title = fileName;
                        }
                        props.put(ContentModel.PROP_TITLE, title);
                        if(pdf_file == null){
                        pdf_file = WorkflowHelper.getRef(complete_path, trialRef, ns);
                        }
                        Map<QName, Serializable> node_props = ns.getProperties(pdf_file);
                        node_props.putAll(props);
                        ns.setProperties(pdf_file, node_props);  
                        }
                    }

                    if (isCompleted) {
                        jobj.put("Editable", false);
                    }

                }
                jobj.put("PdfFile", (pdf_file == null) ? "" : pdf_file);
                jobj.put("TrialRef", trialRef);
                jobj.put("SearchLocation", searchLocation);
            }

            res.getWriter().write(jobj.toString());
        } catch (Exception e) {
            logger.error("Exception :" + e.getMessage());
            throw new AlfrescoRuntimeException(e.getMessage(),e);
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    /**
     * @param serviceRegistry the serviceRegistry to set
     */
    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    /**
     * @param configpath the configpath to set
     */
    public void setConfigpath(String configpath) {
        this.configpath = configpath;
    }

    private String handlePropertyName(String type, String basePropertyName) {
        boolean isType = (type != null && !type.isEmpty());
        return ((isType) ? (type + "_") : "") + basePropertyName;
    }

    public String replaceMethod(String val, Map<String, String> propmap) {
        String r_val = val;
        for (String key : propmap.keySet()) {
            r_val = r_val.replace("${" + key + "}", propmap.get(key));
        }
        return r_val;
    }

}
